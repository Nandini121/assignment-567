public class Internet extends Bill {

	
	//constructor
	public Internet (int billId,String billDate,String billType,double totalBillAmount,String providerName,double internet_GBUsed) {
		super( billId, billDate, billType, totalBillAmount);
	
		this.providerName =providerName;
		this.internet_GBUsed=internet_GBUsed;
	}
		
		String providerName;
		double internet_GBUsed;
		
		//getters and setters
		
		public String getProviderName() {
			return providerName;
		}
		public void setProviderName(String providerName) {
			this.providerName = providerName;
		}
		public double getInternet_GBUsed() {
			return internet_GBUsed;
		}
		public void setInternet_GBUsed(double internet_GBUsed) {
			this.internet_GBUsed = internet_GBUsed;
		}

}
