
public class Hydro  extends Bill {
	
	String agencyName;
	double unitConsumed;
	//constructor
	
	public  Hydro (int billId,String billDate,String billType,double totalBillAmount,String agencyName, double unitConsumed) {
		super (billId,billDate,billType,totalBillAmount);
		this.agencyName =agencyName;
		this.unitConsumed=unitConsumed;
	}
	
	//getters and setters 

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public double getUnitConsumed() {
		return unitConsumed;
	}

	public void setUnitConsumed(double unitConsumed) {
		this.unitConsumed = unitConsumed;
	}

        
	
	

}
