import java.util.Date;

public class Mobile extends Bill{
	
	
	String mobileManufacturerName;
	String planName;
	int mobileNumber;
	double internet_GBUsed;
	double minuteUsed;
	
	// //constructor
		public   Mobile (int billId,String billDate,String billType,double totalBillAmount,String mobileManufacturerName,String planName,int mobileNumber,
				double internet_GBUsed,double minuteUsed) {
			
			super (billId,billDate,billType,totalBillAmount);
			
			this.mobileManufacturerName = mobileManufacturerName;
			this.planName = planName;
			this.mobileNumber = mobileNumber;
			this.internet_GBUsed = internet_GBUsed;
			this.minuteUsed = minuteUsed;
			
		}
		
		//getters and setters

		public String getMobileManufacturerName() {
			return mobileManufacturerName;
		}

		public void setMobileManufacturerName(String mobileManufacturerName) {
			this.mobileManufacturerName = mobileManufacturerName;
		}

		public String getPlanName() {
			return planName;
		}

		public void setPlanName(String planName) {
			this.planName = planName;
		}

		public int getMobileNumber() {
			return mobileNumber;
		}

		public void setMobileNumber(int mobileNumber) {
			this.mobileNumber = mobileNumber;
		}

		public double getInternet_GBUsed() {
			return internet_GBUsed;
		}

		public void setInternet_GBUsed(double internet_GBUsed) {
			this.internet_GBUsed = internet_GBUsed;
		}

		public double getMinuteUsed() {
			return minuteUsed;
		}

		public void setMinuteUsed(double minuteUsed) {
			this.minuteUsed = minuteUsed;
		}

		
}
